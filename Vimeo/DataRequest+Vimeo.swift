//
//  DataRequest+Vimeo.swift
//  Vimeo
//
//  Created by Amal on 2/19/17.
//  Copyright © 2017 Amal. All rights reserved.
//

import Alamofire
import Foundation
import SwiftyJSON

enum VimeoError: Error {
  case network(error: Error)
  case emptyData(reason: String)
  case objectSerialization(reason: String)
  case jsonSerialization(reason: String)
}


extension DataRequest {
  @discardableResult
  func responseVideos(queue: DispatchQueue? = nil,
                      completionHandler: @escaping (DataResponse<[Video]>) -> Void) -> Self {
    
    let responseSerializer = DataResponseSerializer<[Video]> { (request, response, data, error) -> Result<[Video]> in
      guard error == nil else { return .failure(VimeoError.network(error: error!)) }
      
      guard let responseData = data else {
        return .failure(VimeoError.emptyData(reason: "Empty Data"))
      }
      
      var videos: [Video] = []
      let json = JSON(data: responseData)
      if let videoJsons = json.array {
        for vJson in videoJsons {
          if let video = Video(json: vJson) {
            videos.append(video)
          } else {
            return .failure(VimeoError.objectSerialization(reason: "Cannot serialize the object"))
          }
        }
      } else {
        return .failure(VimeoError.jsonSerialization(reason: "JSON Parse error"))
      }
      return .success(videos)
    }
    
    return response(responseSerializer: responseSerializer,
                    completionHandler: completionHandler)
  }
}
