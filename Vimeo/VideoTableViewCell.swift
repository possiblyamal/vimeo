//
//  VideoTableViewCell.swift
//  Vimeo
//
//  Created by Amal on 2/19/17.
//  Copyright © 2017 Amal. All rights reserved.
//

import UIKit

class VideoTableViewCell: UITableViewCell {
  
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var thumbImageView: UIImageView!
  @IBOutlet weak var userLabel: UILabel!
  @IBOutlet weak var uploadedDateLabel: UILabel!
  
}
