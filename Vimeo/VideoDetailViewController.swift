//
//  VideoDetailViewController.swift
//  Vimeo
//
//  Created by Amal on 2/19/17.
//  Copyright © 2017 Amal. All rights reserved.
//

import UIKit


class VideoDetailViewController: UIViewController {
  
  public var video: Video!
  
  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var userImageView: UIImageView!
  @IBOutlet weak var userNameLabel: UILabel!
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var descriptionLabel: UILabel!
  @IBOutlet weak var uploadDateLabel: UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    if let url = self.video.videoThumbImageUrl {
      self.imageView.af_setImage(withURL: url)
    }
    if let userUrl = self.video.userPortraitImageUrl {
      self.userImageView.af_setImage(withURL: userUrl)
    }
    self.titleLabel.text = self.video.title
    self.descriptionLabel.text = self.video.videoDescription
    self.userNameLabel.text = self.video.userName
    if let date = self.video.videoUploadDate {
      self.uploadDateLabel.text = "On \(date)"
    }
  }
  
}
