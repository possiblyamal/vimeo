//
//  VideosModel.swift
//  Vimeo
//
//  Created by Amal on 2/19/17.
//  Copyright © 2017 Amal. All rights reserved.
//

import UIKit

class VideosViewModel {
  
  static let KEY_START_INDEX = "start_index"
  static let KEY_END_INDEX = "end_index"
  
  static let DATA_LOAD_ERROR = Notification.Name("data_loaded_error")
  static let DATA_LOADED = Notification.Name("data_loaded")
  static let PAGE_LOADED = Notification.Name("page_loaded")
  
  private var pageLoaded: Int = 0
  private(set) var isLoading: Bool = false
  private var videos: [Video] = []
  
  var isInitialDataLoaded: Bool {
    get {
      return pageLoaded != 0 || videos.count != 0
    }
  }
  
  public func video(atIndex index: Int) -> Video {
    return videos[index]
  }
  
  public func videoThumbNailUrlForVideo(atIndex index: Int) -> URL? {
    return videos[index].videoThumbImageUrl
  }
  
  public func titleForVideo(atIndex index: Int) -> String? {
    return videos[index].title
  }
  
  public func usernameForVideo(atIndex index: Int) -> String? {
    return videos[index].userName
  }
  
  public func uploadedDateForVideo(atIndex index: Int) -> String? {
    return videos[index].videoUploadDate
  }

  public func descriptionForVideo(atIndex index: Int) -> String? {
    return videos[index].videoDescription
  }
  
  public func numberOfVideos() -> Int {
    return videos.count
  }
  
  
  public func loadInitialData() {
    self.isLoading = true
    VimeoAPIManager.sharedInstance.getVideos(forPage: 1) { (videos, error) in
      self.isLoading = false
      guard error == nil else {
        self.sendErrorNotification()
        return
      }
      self.pageLoaded = 1
      self.videos = videos!
      self.sendDataLoadedNotification()
    }
  }
  
  public func loadNextPage() {
    // The Current API does not return data for pages greater than 3.
    if pageLoaded >= 3 {
      return
    }
    self.isLoading = true
    VimeoAPIManager.sharedInstance.getVideos(forPage: self.pageLoaded + 1) { (videos, error) in
      self.isLoading = false
      guard error == nil else {
        return
      }
      self.pageLoaded += 1
      let oldSize = self.videos.count
      self.videos += videos!
      self.sendPageLoadedNotification(startIndex: oldSize, endIndex: self.videos.count - 1)
    }
  }
  
  
  // MARK:- Notification Methods
  
  private func sendErrorNotification() {
    NotificationCenter.default.post(Notification(name: VideosViewModel.DATA_LOAD_ERROR))
  }
  
  private func sendDataLoadedNotification() {
    NotificationCenter.default.post(Notification(name: VideosViewModel.DATA_LOADED))
  }
  
  private func sendPageLoadedNotification(startIndex: Int, endIndex: Int) {
    let userInfo = [VideosViewModel.KEY_START_INDEX : startIndex,
                    VideosViewModel.KEY_END_INDEX : endIndex]
    let notification = Notification(name: VideosViewModel.PAGE_LOADED,
                                    object: nil, userInfo: userInfo)
    NotificationCenter.default.post(notification)
  }

}
