//
//  ViewController.swift
//  Vimeo
//
//  Created by Amal on 2/19/17.
//  Copyright © 2017 Amal. All rights reserved.
//

import Alamofire
import AlamofireImage
import UIKit

class VideosViewController: UITableViewController {
  
  let kVideoCellIdentifier = "VideoTableViewCell"
  private let model = VideosViewModel()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    if self.refreshControl == nil {
      self.refreshControl = UIRefreshControl()
      self.refreshControl?.addTarget(self, action: #selector(loadFirstPage), for: .valueChanged)
    }
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.observeModel()
    if !model.isInitialDataLoaded {
      self.loadFirstPage()
    }
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    self.stopObservingModel()
  }
  
  func loadFirstPage() {
    if !model.isLoading{
      model.loadInitialData()
    } else {
      self.refreshControl?.endRefreshing()
    }
  }
  
  
  // Mark:- ModelNotification methods
  fileprivate func observeModel() {
    NotificationCenter.default.addObserver(self, selector: #selector(initialDataLoaded(notification:)),
                                           name: VideosViewModel.DATA_LOADED,
                                           object: nil)
    
    NotificationCenter.default.addObserver(self, selector: #selector(pageLoaded(notification:)),
                                           name: VideosViewModel.PAGE_LOADED,
                                           object: nil)
    
    NotificationCenter.default.addObserver(self, selector: #selector(dataLoadError(notification:)),
                                           name: VideosViewModel.DATA_LOAD_ERROR,
                                           object: nil)

  }
  
  fileprivate func stopObservingModel() {
    NotificationCenter.default.removeObserver(self, name: VideosViewModel.DATA_LOADED, object: nil)
    NotificationCenter.default.removeObserver(self, name: VideosViewModel.PAGE_LOADED, object: nil)
    NotificationCenter.default.removeObserver(self, name: VideosViewModel.DATA_LOAD_ERROR, object: nil)
  }

  
  func initialDataLoaded(notification: Notification) {
    if let _ = self.refreshControl?.isRefreshing {
      self.refreshControl?.endRefreshing()
    }
    self.tableView.reloadData()
  }
  
  func dataLoadError(notification: Notification) {
    let av = UIAlertController(title: nil, message: "Error Loading Data. Please try in some time.", preferredStyle: .alert)
    av.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
    self.present(av, animated: true, completion: nil)
  }
  
  func pageLoaded(notification: Notification) {
    let userInfo = notification.userInfo
    let startIndex = userInfo?[VideosViewModel.KEY_START_INDEX] as? Int
    let endIndex = userInfo?[VideosViewModel.KEY_END_INDEX] as? Int
    if startIndex != nil && endIndex != nil {
      var indexPaths: [IndexPath] = []
      for i in startIndex!..<endIndex!+1 {
        indexPaths.append(IndexPath(item: i, section: 0))
      }
      self.tableView.beginUpdates()
      self.tableView.insertRows(at: indexPaths, with: .automatic)
      self.tableView.endUpdates()
    } else {
      self.tableView.reloadData()
    }
  }
  
  
  // MARK:- UITableViewDataSource
  override func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return model.numberOfVideos()
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: kVideoCellIdentifier, for: indexPath) as! VideoTableViewCell
    cell.thumbImageView.image = nil
    return cell
  }
  
  
  // MARK:- UITableViewDelegate
  override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    
    let videoCell = cell as! VideoTableViewCell
    videoCell.titleLabel.text = self.model.titleForVideo(atIndex: indexPath.row)
    videoCell.userLabel.text  = self.model.usernameForVideo(atIndex: indexPath.row)
    videoCell.uploadedDateLabel.text = self.model.uploadedDateForVideo(atIndex: indexPath.row)
    if let url = self.model.videoThumbNailUrlForVideo(atIndex: indexPath.row) {
      videoCell.thumbImageView.af_setImage(withURL: url)
    }
    
    let rowsToLoadFromBottom = 5
    let rows = model.numberOfVideos()
    if !model.isLoading && indexPath.row >= (rows - rowsToLoadFromBottom) {
      self.model.loadNextPage()
    }
  }
  
  // MARK:- Segue Methods
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    let vc = segue.destination as! VideoDetailViewController
    if let indexPath = self.tableView.indexPath(for: sender as! UITableViewCell) {
      vc.video = self.model.video(atIndex: indexPath.row)
    }
  }
  
}

