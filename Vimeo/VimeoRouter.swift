//
//  VimeoRouter.swift
//  Vimeo
//
//  Created by Amal on 2/19/17.
//  Copyright © 2017 Amal. All rights reserved.
//

import Alamofire

enum VimeoRouter: URLRequestConvertible {
  case getVideos(Int)
  
  static let baseURLString = "https://vimeo.com/api/v2"
  
  public func asURLRequest() throws -> URLRequest {
    let attributes: (path: String, params: Parameters) = {
      switch self {
      case let .getVideos(page):
        return ("/album/58/videos.json", ["page" : page])
      }
    }()
    
    let url = try VimeoRouter.baseURLString.asURL()
    let request = URLRequest(url: url.appendingPathComponent(attributes.path))
    return try URLEncoding.default.encode(request, with: attributes.params)
  }
}
