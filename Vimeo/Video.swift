//
//  Video.swift
//  Vimeo
//
//  Created by Amal on 2/19/17.
//  Copyright © 2017 Amal. All rights reserved.
//

import SwiftyJSON
import UIKit

class Video {
  
  var title: String?
  var videoDescription: String?
  var userName: String?
  var userPortraitImageUrl: URL?
  var videoThumbImageUrl: URL?
  var videoUploadDate: String?
  
  var description: String {
    return "Video : {title : \(title), description: \(videoDescription), userName: \(userName)}"
  }
  
  init?(json: JSON) {
    self.title = json["title"].string
    self.videoDescription = json["description"].string
    self.userName = json["user_name"].string
    
    do {
      self.userPortraitImageUrl = try json["user_portrait_medium"].string?.asURL()
    } catch {
      self.userPortraitImageUrl = nil
    }
    
    do {
      self.videoThumbImageUrl = try json["thumbnail_medium"].string?.asURL()
    } catch {
      self.videoThumbImageUrl = nil
    }
    
    self.videoUploadDate = json["upload_date"].string
  }
  
}
