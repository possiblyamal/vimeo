//
//  VimeoAPIManager.swift
//  Vimeo
//
//  Created by Amal on 2/19/17.
//  Copyright © 2017 Amal. All rights reserved.
//

import Foundation
import Alamofire

class VimeoAPIManager {
  static let sharedInstance = VimeoAPIManager()
  
  public func getVideos(forPage page: Int,
                        completionHandler: @escaping ([Video]?, Error?) -> Void) {
    
    Alamofire.request(VimeoRouter.getVideos(page)).responseVideos { (result) in
      completionHandler(result.value, result.error)
    }
  }
}
